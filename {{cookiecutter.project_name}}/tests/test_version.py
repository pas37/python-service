import os
import sys

import setuptools_scm

VERSION_FILE = os.path.join(
    os.path.dirname(os.path.dirname(os.path.realpath(__file__))),
    "{{cookiecutter.project_name}}",
    "_version.py",
)


def test_version_creates_version_file_if_missing():
    if os.path.exists(VERSION_FILE):
        os.remove(VERSION_FILE)

    if "{{cookiecutter.project_name}}.version" in sys.modules:
        del sys.modules["{{cookiecutter.project_name}}.version"]
    import {{cookiecutter.project_name}}.version

    assert os.path.exists(VERSION_FILE)


def test_version_update_version_file_if_exists():
    previous_mtime = os.path.getmtime(VERSION_FILE)

    if "{{cookiecutter.project_name}}.version" in sys.modules:
        del sys.modules["{{cookiecutter.project_name}}.version"]
    import {{cookiecutter.project_name}}.version

    assert os.path.getmtime(VERSION_FILE) > previous_mtime


def test_version_has_proper_version():
    version = setuptools_scm.get_version()

    if "{{cookiecutter.project_name}}.version" in sys.modules:
        del sys.modules["{{cookiecutter.project_name}}.version"]
    from {{cookiecutter.project_name}}.version import __version__

    assert __version__ == version


def test_version_does_not_require_setuptools_scm(monkeypatch):
    monkeypatch.setitem(sys.modules, "setuptools_scm", None)

    if "{{cookiecutter.project_name}}.version" in sys.modules:
        del sys.modules["{{cookiecutter.project_name}}.version"]
    from {{cookiecutter.project_name}}.version import __version__

    assert __version__ is not None
