import {{cookiecutter.project_name}}


def test_{{cookiecutter.project_name}}_has_version():
    assert {{cookiecutter.project_name}}.__version__ is not None
