"""
Get application version

If the setuptools_scm package exists calculate the version and write
to the _version.py file.  Then import version from _version.py

NOTE: _version.py is not committed to the repository.  The 'pdm
version' command also updates _version.py.
"""
import os

__all__ = ["__version__"]

try:
    import setuptools_scm  # type: ignore

    version_file = os.path.join(
        os.path.dirname(os.path.realpath(__file__)), "_version.py"
    )
    setuptools_scm.get_version(write_to=version_file)
except ImportError:
    pass

from ._version import __version__  # pylint: disable=wrong-import-position
